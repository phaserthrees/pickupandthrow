﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ItemSelector : NetworkBehaviour
{
    public List<GameObject> localItem;
    public List<GameObject> multiplayItem;
    public bool itemsLocked;
    public int currentItem = 0;
    public int maxItems = 3;
    private float travel;
    public int nextActionSpeed = 10;
    public bool nextAction = false;
    public Transform EquipmentContainer;
    public Transform ShoulderContainer;
    [SerializeField] private Animator animator = null;


    public string currentItemType;

    [ClientCallback]
    void Update()
    {

        float scrollValue = Input.GetAxis("Mouse ScrollWheel");

        if (nextAction)
        {
            travel += Time.deltaTime * nextActionSpeed;
            if (travel > 2f)
            {
                nextAction = false;
                travel = 0;
            }
        }

        if (scrollValue > 0f && !nextAction && !itemsLocked)
        {
            selectNextItem();
            nextAction = true;
        }
        else if (scrollValue < 0f && !nextAction && !itemsLocked)
        {
            selectPreviousItem();
            nextAction = true;
        }

    }

    public void addItem(GameObject _multiplayerItem, GameObject _localItem)
    {
        animator.SetBool("HasItems", true);
        if (currentItem > 0)
        {
            localItem[currentItem - 1].SetActive(false);
            CmdHideItem(multiplayItem[currentItem - 1]);
        }

        currentItemType = _multiplayerItem.tag;
        currentItem += 1;
        localItem.Add(_localItem);
        CmdAddItem(_multiplayerItem);
    }


    public void removeItem()
    {
        if (currentItem > 0)
        {
            localItem.RemoveAt(currentItem - 1);
            CmdRemoveItem(currentItem);
            currentItem -= 1;
            if (localItem.Count > 0)
            {
                if (currentItem == 0)
                {
                    currentItem = 1;
                }
                localItem[currentItem - 1].SetActive(true);
                if (localItem[currentItem - 1].tag == "Flashlight")
                {
                    localItem[currentItem - 1].GetComponent<MeshRenderer>().enabled = true;
                }
                CmdShowItem(multiplayItem[currentItem - 1]);
                currentItemType = localItem[currentItem - 1].tag;
            }
            else {
                animator.SetBool("HasItems", false);
            }
        }
    }

    public void selectNextItem()
    {
        if (localItem.Count > 1)
        {
            if (localItem[currentItem - 1].tag == "Flashlight")
            {
                localItem[currentItem - 1].GetComponent<MeshRenderer>().enabled = false;
                CmdFlashlight(multiplayItem[currentItem - 1], true);
            }
            else {
                localItem[currentItem - 1].SetActive(false);
                CmdHideItem(multiplayItem[currentItem - 1]);
            }

            currentItem += 1;

            if (currentItem > localItem.Count)
            {
                currentItem = 1;
            }
            localItem[currentItem - 1].SetActive(true);
            if (localItem[currentItem - 1].tag == "Flashlight") 
            {
                localItem[currentItem - 1].GetComponent<MeshRenderer>().enabled = true;
                CmdFlashlight(multiplayItem[currentItem - 1], false);
            }

            CmdShowItem(multiplayItem[currentItem - 1]);
            currentItemType = multiplayItem[currentItem - 1].tag;

        }
    }

    public void selectPreviousItem()
    {
        if (localItem.Count > 1)
        {

            if (localItem[currentItem - 1].tag == "Flashlight")
            {
                localItem[currentItem - 1].GetComponent<MeshRenderer>().enabled = false;
                CmdFlashlight(multiplayItem[currentItem - 1], true);
            }
            else
            {
                localItem[currentItem - 1].SetActive(false);
                CmdHideItem(multiplayItem[currentItem - 1]);
            }

            currentItem -= 1;

            if (currentItem == 0)
            {
                currentItem = localItem.Count;
            }

            localItem[currentItem - 1].SetActive(true);
            if (localItem[currentItem - 1].tag == "Flashlight")
            {
                localItem[currentItem - 1].GetComponent<MeshRenderer>().enabled = true;
                CmdFlashlight(multiplayItem[currentItem - 1], false);
            }

            CmdShowItem(multiplayItem[currentItem - 1]);
            currentItemType = multiplayItem[currentItem - 1].tag;

        }
    }

    public void hideCurrentItem()
    {
        if (localItem.Count > 0) { 
            itemsLocked = true;

            if (localItem[currentItem - 1].tag == "Flashlight")
            {
                localItem[currentItem - 1].GetComponent<MeshRenderer>().enabled = false;
                CmdFlashlight(multiplayItem[currentItem - 1], true);
            }
            else {
                localItem[currentItem - 1].SetActive(false);
                CmdHideItem(multiplayItem[currentItem - 1]);
            }
        }
    }

    public void showCurrentItem()
    {
        if (localItem.Count > 0)
        {
            itemsLocked = false;
            localItem[currentItem - 1].SetActive(true);
            if (localItem[currentItem - 1].tag == "Flashlight")
            {
                localItem[currentItem - 1].GetComponent<MeshRenderer>().enabled = true;
                CmdFlashlight(multiplayItem[currentItem - 1], false);
            }
            CmdShowItem(multiplayItem[currentItem - 1]);
        }
    }


    [Command]
    private void CmdHideItem(GameObject hideitem)
    {
        NetworkIdentity networkIdentityHideItem = hideitem.GetComponent<NetworkIdentity>();

        if (networkIdentityHideItem != null)
        {
           /* if (!networkIdentityHideItem.hasAuthority)
            {*/
                networkIdentityHideItem.AssignClientAuthority(base.connectionToClient);
                //Debug.Log("hide " + hideitem.tag);
                RcpHideItem(hideitem);
            //}
        }

    }

    [Command]
    public void CmdShowItem(GameObject showitem)
    {
        NetworkIdentity networkIdentityShowItem = showitem.GetComponent<NetworkIdentity>();
       
        if (networkIdentityShowItem != null)
        {

           /* if (!networkIdentityShowItem.hasAuthority)
            {*/
                networkIdentityShowItem.AssignClientAuthority(base.connectionToClient);

                RcpShowItem(showitem);
           // }
        }

    }

    [Command]
    private void CmdAddItem(GameObject additem)
    {
        NetworkIdentity networkIdentityAddItem = additem.GetComponent<NetworkIdentity>();
        if (networkIdentityAddItem != null)
        {
           /* if (!networkIdentityAddItem.hasAuthority)
            {*/
                networkIdentityAddItem.AssignClientAuthority(base.connectionToClient);
                RcpAddItem(additem);
           // }
        }

    }

    [Command]
    private void CmdRemoveItem(int curitem)
    {
        RcpRemoveItem(curitem);
    }

    [Command]
    public void CmdFlashlight(GameObject equip, bool shoulder)
    {
        NetworkIdentity networkIdentityFlashlight = equip.GetComponent<NetworkIdentity>();

        if (networkIdentityFlashlight != null)
        {
            if (!networkIdentityFlashlight.hasAuthority)
            {
                networkIdentityFlashlight.AssignClientAuthority(base.connectionToClient);
                RcpFlashlight(equip, shoulder);
            }
        }
    }


    [ClientRpc]
    void RcpFlashlight(GameObject equip, bool shoulder)
    {
        if (!shoulder)
        {
            equip.transform.SetParent(EquipmentContainer);
        }
        else
        {
            equip.transform.SetParent(ShoulderContainer);
        }
    }

    [ClientRpc]
    void RcpHideItem(GameObject hideitem)
    {

        hideitem.SetActive(false);
    }

    [ClientRpc]
    void RcpShowItem(GameObject showitem)
    {        
        if (!isLocalPlayer)
        {
            showitem.SetActive(true);
            showitem.transform.localPosition = Vector3.zero;
            showitem.transform.localRotation = Quaternion.Euler(Vector3.zero);

        }
        else {
            showitem.SetActive(false);
        }
    }

    [ClientRpc]
    void RcpAddItem(GameObject additem)
    {
        multiplayItem.Add(additem);

        if (!isLocalPlayer)
        {
            additem.transform.localPosition = Vector3.zero;
            additem.transform.localRotation = Quaternion.Euler(Vector3.zero);

        }

    }

    [ClientRpc]
    void RcpRemoveItem(int curitem)
    {
        multiplayItem.RemoveAt(curitem - 1);
    }

}
