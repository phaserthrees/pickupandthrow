﻿using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class PickupCameraDevice : NetworkBehaviour
{
    public Transform player, fpsCam, EquipmentContainer;
    public float dropForwardForce, dropUpwardForce;

    [Header("Raycast Parameters")]
    [SerializeField] private int rayLength = 5;


    [Header("Key Codes")]
    [SerializeField] private KeyCode pickupKey = KeyCode.Mouse0;
    [SerializeField] private KeyCode dropKey = KeyCode.E;

    private bool isCrosshairActive;
    private Image crosshair = null;
    private bool doOnce;
    public GameObject FPScameraDevice;
    public ItemSelector itemSelector;
    public Sprite grab;
    public Sprite point;
    public bool cameraDeviceEquiped;
    private GameObject lastObjectInteraction;
    public GameObject pickedObject;
    public bool isPlacing;
    [SerializeField]
    public LayerMask IgnoreMe;
    private float mouseWheelRotation;

    [SerializeField] private const string interactableCameraDeviceTag = "CameraDevice";


    private void Start()
    {
        crosshair = GameObject.FindGameObjectWithTag("Crosshair").GetComponent<Image>();

    }

    [ClientCallback]
    private void Update()
    {

        if (!isLocalPlayer) return;



        // Drop Object
        if (Input.GetKeyDown(dropKey) && cameraDeviceEquiped && itemSelector.currentItemType == interactableCameraDeviceTag && !itemSelector.nextAction && !itemSelector.itemsLocked && isPlacing)
        {
            if (pickedObject == null) return;
            cameraDeviceEquiped = false;
            itemSelector.removeItem();
            CmdDrop(pickedObject , true);
            itemSelector.nextAction = true;
        }


        // Place Object
        if (isPlacing) { 
            MoveCurrentObjectToMouse();
            RotateFromMouseWheel();
            ReleaseIfClicked();
        }

        if (Input.GetMouseButtonDown(1) && cameraDeviceEquiped && itemSelector.currentItemType == interactableCameraDeviceTag && !itemSelector.nextAction)
        {
            if (!isPlacing)
            {
                pickedObject.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
                pickedObject.SetActive(true);
                FPScameraDevice.SetActive(false);
                itemSelector.itemsLocked = true;
                isPlacing = true;
                CmdPlaceActive(pickedObject);
            }
            else 
            {
                pickedObject.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 1f);
                pickedObject.transform.SetParent(EquipmentContainer);
                pickedObject.transform.localPosition = new Vector3(0.03f, 0.03f, 0);
                pickedObject.transform.localRotation = Quaternion.Euler(new Vector3(130f, 60f, 155f));
                pickedObject.SetActive(false);
                FPScameraDevice.SetActive(true);
                itemSelector.itemsLocked = false;
                isPlacing = false;
                CmdPlaceCancel(pickedObject);
            }
        }

        if (Camera.main != null)
        {
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, rayLength))
            {

                if (hit.collider.CompareTag(interactableCameraDeviceTag))
                {
                    if (!doOnce)
                    {
                        lastObjectInteraction = hit.collider.gameObject;
                        CrosshairChange(true);
                    }

                    isCrosshairActive = true;
                    doOnce = true;
                    if (hit.collider.name != lastObjectInteraction.name)
                    {
                        doOnce = false;
                    }


                    // Pickup Object
                    if (Input.GetKeyDown(pickupKey) && !cameraDeviceEquiped && itemSelector.maxItems > itemSelector.localItem.Count && !itemSelector.itemsLocked)
                    {
                        cameraDeviceEquiped = true;
                        if (!hasAuthority) { return; }
                        itemSelector.addItem(hit.collider.gameObject, FPScameraDevice);
                        CmdPickUp(hit.collider.gameObject);
                    }
                }
                else
                {
                    if (isCrosshairActive)
                    {
                        CrosshairChange(false);
                        doOnce = false;
                    }
                }
            }
        }

    }
    void CrosshairChange(bool on)
    {
        if (on && !doOnce)
        {
            crosshair.sprite = grab;
            
        }
        else
        {
            crosshair.sprite = point;
            isCrosshairActive = false;
        }
    }

    public void ActivateObject()
    {
        pickedObject.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
        pickedObject.SetActive(true);
        FPScameraDevice.SetActive(false);
        itemSelector.itemsLocked = true;
        isPlacing = true;
        pickedObject.transform.SetParent(null);
    }

    public void CancelObject(GameObject pickedObject)
    {

        pickedObject.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 1f);
        pickedObject.transform.SetParent(EquipmentContainer);
        pickedObject.transform.localPosition = new Vector3(0.03f, 0.03f, 0);
        pickedObject.transform.localRotation = Quaternion.Euler(new Vector3(130f, 60f, 155f));
        pickedObject.SetActive(false);
        FPScameraDevice.SetActive(true);
        itemSelector.itemsLocked = false;
        isPlacing = false;
        pickedObject.transform.SetParent(null);
    }

    private void MoveCurrentObjectToMouse()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo, 1000f, ~IgnoreMe))
        {
            pickedObject.transform.position = hitInfo.point;
            pickedObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);
        }
    }

    private void RotateFromMouseWheel()
    {
        mouseWheelRotation += Input.mouseScrollDelta.y;
        pickedObject.transform.Rotate(Vector3.up, mouseWheelRotation * 10f);
    }

    private void ReleaseIfClicked()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CmdReleaseIfClicked(pickedObject);
        }
    }



    [Command]
    private void CmdPlaceActive(GameObject item)
    {
        RcpPlaceActive(item);
    }

    [ClientRpc]
    void RcpPlaceActive(GameObject equip)
    {
        equip.transform.SetParent(null);
        pickedObject = equip;
    }

    [Command]
    private void CmdPlaceCancel(GameObject item)
    {
        RcpPlaceCancel(item);
    }

    [ClientRpc]
    void RcpPlaceCancel(GameObject equip)
    {
        equip.transform.SetParent(EquipmentContainer);
        pickedObject = equip;
    }


    [Command]
    private void CmdDrop(GameObject item , bool auth)
    {
        NetworkIdentity networkIdentity = item.GetComponent<NetworkIdentity>();

        if (networkIdentity != null)
        {
            if (auth) 
            {
                RpcDrop(item);
                networkIdentity.RemoveClientAuthority();
            }
            else 
            {
                RpcDrop(item);
            }

         
        }

    }

    [Command]
    private void CmdPickUp(GameObject item)
    {
        NetworkIdentity networkIdentity = item.GetComponent<NetworkIdentity>();
        
        if (networkIdentity != null)
        {
            Debug.Log("networkIdentity: " + networkIdentity.hasAuthority);
            if (!networkIdentity.hasAuthority)
            {
                networkIdentity.AssignClientAuthority(base.connectionToClient);
            }
            RpcPickup(item);
        }

    }

    [Command]
    private void CmdReleaseIfClicked(GameObject item)
    {
        NetworkIdentity networkIdentity = item.GetComponent<NetworkIdentity>();

        if (networkIdentity != null)
        {
                RpcReleaseIfClicked(item);
                networkIdentity.RemoveClientAuthority();
        }

    }


    [ClientRpc]
    void RpcReleaseIfClicked(GameObject current)
    {

        current.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 1f);
        current.transform.SetParent(null);
        isPlacing = false;
        cameraDeviceEquiped = false;
        itemSelector.removeItem();
        itemSelector.itemsLocked = false;
        pickedObject = null;
    }

    [ClientRpc]
    void RpcPickup(GameObject equip)
    {

        equip.transform.SetParent(EquipmentContainer);
        equip.transform.localPosition = new Vector3(0.03f, 0.03f, 0);
        equip.transform.localRotation = Quaternion.Euler(new Vector3(130f, 60f, 155f));

        if (isLocalPlayer)
        {
            equip.SetActive(false);
            FPScameraDevice.SetActive(true);
        }


        equip.GetComponent<Rigidbody>().isKinematic = true;
        equip.GetComponent<BoxCollider>().isTrigger = true;
        pickedObject = equip;
    }

    [ClientRpc]
    void RpcDrop(GameObject equip)
    {

        if (isLocalPlayer)
        {
            equip.SetActive(true);
            FPScameraDevice.SetActive(false);
        }

        //Set scale back
        equip.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //Set parent to null
        equip.transform.SetParent(null);

        //Make Rigidbody not kinematic and BoxCollider normal
        equip.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        equip.gameObject.GetComponent<BoxCollider>().isTrigger = false;

        //Gun carries momentum of player
        equip.GetComponent<Rigidbody>().velocity = player.GetComponent<Rigidbody>().velocity;

        //AddForce
        equip.GetComponent<Rigidbody>().AddForce(fpsCam.forward * dropForwardForce, ForceMode.Impulse);
        equip.GetComponent<Rigidbody>().AddForce(fpsCam.up * dropUpwardForce, ForceMode.Impulse);
        //Add random rotation
        float random = Random.Range(-1f, 1f);
        equip.GetComponent<Rigidbody>().AddTorque(new Vector3(random, random, random) * 10);

        pickedObject = null;

        equip.GetComponent<Rigidbody>().isKinematic = false;
        equip.GetComponent<BoxCollider>().isTrigger = false;
    }


}