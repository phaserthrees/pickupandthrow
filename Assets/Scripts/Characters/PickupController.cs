﻿using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class PickupController : NetworkBehaviour
{
    public Transform player, bagContainer, fpsCam;
    public float dropForwardForce, dropUpwardForce;

    [Header("Raycast Parameters")]
    [SerializeField] private int rayLength = 5;


    [Header("Key Codes")]
    [SerializeField] private KeyCode pickupKey = KeyCode.Mouse0;
    [SerializeField] private KeyCode dropKey = KeyCode.E;

    private bool isCrosshairActive;
    private Image crosshair = null;
    private bool doOnce;
    public Sprite grab;
    public Sprite point;
    private bool equipped;
    private GameObject lastObjectInteraction;

    public GameObject pickedObject;

    [SerializeField] private const string interactableItemTag = "Item";


    private void Start()
    {
        crosshair = GameObject.FindGameObjectWithTag("Crosshair").GetComponent<Image>();
    }


    [ClientCallback]
    private void Update()
    {

        if (!isLocalPlayer) return;

        if (Input.GetKeyDown(dropKey) && equipped)
        {
            if (pickedObject == null) return;
            equipped = false;
            CmdDrop(pickedObject);
        }

        if (Camera.main != null)
        {
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            RaycastHit hit;
 
            if (Physics.Raycast(ray, out hit, rayLength))
            {

                if (hit.collider.CompareTag(interactableItemTag))
                {
                    if (!doOnce)
                    {
                        lastObjectInteraction = hit.collider.gameObject;
                        CrosshairChange(true);
                    }

                    isCrosshairActive = true;
                    doOnce = true;
                    if (hit.collider.name != lastObjectInteraction.name || equipped)
                    {
                        doOnce = false;
                    }

                    if (Input.GetKeyDown(pickupKey) && !equipped)
                    {
                        equipped = true;
                        if (!hasAuthority) { return; }
                        CmdPickUp(hit.collider.gameObject);
                    }
                }
                else
                {
                    if (isCrosshairActive)
                    {
                        CrosshairChange(false);
                        doOnce = false;
                    }
                }
            }
        }
    }
    void CrosshairChange(bool on)
    {
        if (on && !doOnce)
        {
            crosshair.sprite = grab;
        }
        else
        {
            crosshair.sprite = point;
            isCrosshairActive = false;
        }
    }

    [Command]
    private void CmdDrop(GameObject item)
    {
        NetworkIdentity networkIdentity = item.GetComponent<NetworkIdentity>();

        if (networkIdentity != null)
        {
            RpcDrop(item);
            networkIdentity.RemoveClientAuthority();
        }

    }

    [Command]
    private void CmdPickUp(GameObject item)
    {

        NetworkIdentity networkIdentity = item.GetComponent<NetworkIdentity>();

        if (networkIdentity != null)
        {
            if (!networkIdentity.hasAuthority)
            {
                networkIdentity.AssignClientAuthority(base.connectionToClient);
            }
            RpcPickup(item);
        }

    }

    [ClientRpc]
    void RpcPickup(GameObject equip)
    {
        

        equip.GetComponent<Rigidbody>().isKinematic = true;
        equip.GetComponent<BoxCollider>().isTrigger = true;
        equip.transform.SetParent(bagContainer);
        equip.transform.localPosition = Vector3.zero;
        equip.transform.localRotation = Quaternion.Euler(Vector3.zero);
        pickedObject = equip;

    }

    [ClientRpc]
    void RpcDrop(GameObject equip)
    {


        //Set parent to null
        equip.transform.SetParent(null);

        //Make Rigidbody not kinematic and BoxCollider normal
        equip.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        equip.gameObject.GetComponent<BoxCollider>().isTrigger = false;

        //Gun carries momentum of player
        equip.GetComponent<Rigidbody>().velocity = player.GetComponent<Rigidbody>().velocity;

        //AddForce
        equip.GetComponent<Rigidbody>().AddForce(fpsCam.forward * dropForwardForce, ForceMode.Impulse);
        equip.GetComponent<Rigidbody>().AddForce(fpsCam.up * dropUpwardForce, ForceMode.Impulse);
        //Add random rotation
        float random = Random.Range(-1f, 1f);
        equip.GetComponent<Rigidbody>().AddTorque(new Vector3(random, random, random) * 10);
        
        pickedObject = null;


    }


}