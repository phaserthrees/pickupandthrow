﻿using UnityEngine;
using Mirror;

public class Cube : NetworkBehaviour
{
    [SyncVar]
    public GameObject parent;



    private void Update()
    {
        if (parent != null)
        {
            /*transform.position = parent.transform.position;
            transform.rotation = parent.transform.rotation;*/
            transform.position = parent.transform.Find("Main Camera").Find("Local").Find("Bag").transform.position;
            transform.rotation = parent.transform.Find("Main Camera").Find("Local").Find("Bag").transform.rotation;

            this.GetComponent<Rigidbody>().isKinematic = true;
        }
        else {
            this.GetComponent<Rigidbody>().isKinematic = false;
        }

    }


}