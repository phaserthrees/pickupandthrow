﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Hand : NetworkBehaviour
{
    public GameObject PickUp;
    public Transform fpsCam;
    public float dropForwardForce, dropUpwardForce;

    private void Start()
    {
        
    }


    private void Update()
    {

        // Drop Object
        if (Input.GetKeyDown(KeyCode.E))
        {
            CmdDropOff();
        }
    }


    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Item")) return;
        CmdPickUp(other.gameObject);

    }

    [Command]
    private void CmdPickUp(GameObject cube)
    {
        PickUp = cube;
        PickUp.GetComponent<NetworkIdentity>().AssignClientAuthority(connectionToClient);
        PickUp.GetComponent<Cube>().parent = gameObject;

    }

    [Command]
    public void CmdDropOff()
    {
        PickUp.GetComponent<Cube>().parent = null;
        PickUp.GetComponent<Rigidbody>().isKinematic = false;
        PickUp.GetComponent<Rigidbody>().AddForce(fpsCam.forward * dropForwardForce, ForceMode.Impulse);
        PickUp.GetComponent<Rigidbody>().AddForce(fpsCam.up * dropUpwardForce, ForceMode.Impulse);
        //Add random rotation
        float random = Random.Range(-1f, 1f);
        PickUp.GetComponent<Rigidbody>().AddTorque(new Vector3(random, random, random) * 10);
    }


}
